import { expect, test } from "@playwright/test"

// test("index page has expected h2", async ({ page }) => {
//   await page.goto("/")
//   expect(await page.textContent("h2")).toContain("API de LexImpact Territoires")
// })

test('Autocomplete commune "75007 PARIS"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=75007 PARIS", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("75107")
})

test('Autocomplete commune "PARIS 7"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=PARIS 7", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("75107")
})

test('Autocomplete commune "75107"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=75107", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("75107")
})

test('Autocomplete commune "92 CHATILLON"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=92 CHATILLON", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].autocompletion).toBe("92320 CHATILLON")
})

test('Autocomplete commune "Cœuvres" (e dans l\'o)', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=Cœuvres", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("02201")
})

test('Autocomplete commune "SAINT MALO"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=SAINT MALO", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "35400 SAINT MALO"', async ({ request }) => {
  const response = await request.get(
    "/communes/autocomplete?q=35400 SAINT MALO",
    { headers: { Accept: "application/json" } },
  )
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "35 SAINT MALO"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=35 SAINT MALO", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "PARAME"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=PARAME", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "35400 PARAME"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=35400 PARAME", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "35 PARAME"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=35 PARAME", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("35288")
})

test('Autocomplete commune "SAINT PYTHON" without optional field', async ({
  request,
}) => {
  const response = await request.get("/communes/autocomplete?q=SAINT PYTHON", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("59541")
  expect(suggestions[0].commune).toBe(undefined)
  expect(suggestions[0].distributions_postales).toBe(undefined)
})

test('Autocomplete commune "ST PYTHON" with field "commune"', async ({
  request,
}) => {
  const response = await request.get(
    "/communes/autocomplete?field=commune&q=ST PYTHON",
    { headers: { Accept: "application/json" } },
  )
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("59541")
  expect(suggestions[0].commune).not.toBe(undefined)
  expect(suggestions[0].commune.COM).toBe("59541")
  expect(suggestions[0].distributions_postales).toBe(undefined)
})

test('Autocomplete commune "ST PYTHON" with field "distributions_postales"', async ({
  request,
}) => {
  const response = await request.get(
    "/communes/autocomplete?field=distributions_postales&q=ST PYTHON",
    { headers: { Accept: "application/json" } },
  )
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("59541")
  expect(suggestions[0].communes).toBe(undefined)
  expect(suggestions[0].distributions_postales).not.toBe(undefined)
  expect(suggestions[0].distributions_postales.length).toBe(1)
  expect(suggestions[0].distributions_postales[0].code_commune_insee).toBe(
    "59541",
  )
})

test('Autocomplete commune "ST PYTHON" with fields "communes" & "distributions_postales"', async ({
  request,
}) => {
  const response = await request.get(
    "/communes/autocomplete?field=commune&field=distributions_postales&q=ST PYTHON",
    { headers: { Accept: "application/json" } },
  )
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  expect(suggestions[0].code).toBe("59541")
  expect(suggestions[0].commune).not.toBe(undefined)
  expect(suggestions[0].commune.COM).toBe("59541")
  expect(suggestions[0].distributions_postales).not.toBe(undefined)
  expect(suggestions[0].distributions_postales.length).toBe(1)
  expect(suggestions[0].distributions_postales[0].code_commune_insee).toBe(
    "59541",
  )
})

test('Autocomplete partial commune "MONTREUI"', async ({ request }) => {
  const response = await request.get("/communes/autocomplete?q=MONTREUI", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const { suggestions } = await response.json()
  for (const suggestion of suggestions) {
    expect(suggestion.autocompletion).toMatch(/ MONTREUI/)
  }
})

test('Retrieve commune by Insee code "75107"', async ({ request }) => {
  const response = await request.get("/communes/75107", {
    headers: { Accept: "application/json" },
  })
  expect(response.ok()).toBeTruthy()
  const result = await response.json()
  expect(result.commune.COM).toBe("75107")
  expect(result.distributions_postales).toBe(undefined)
})

test('Retrieve commune by Insee code "75107" with field "distributions_postales"', async ({
  request,
}) => {
  const response = await request.get(
    "/communes/75107?field=distributions_postales",
    { headers: { Accept: "application/json" } },
  )
  expect(response.ok()).toBeTruthy()
  const result = await response.json()
  expect(result.commune.COM).toBe("75107")
  expect(result.distributions_postales).not.toBe(undefined)
  expect(result.distributions_postales.length).toBe(1)
  expect(result.distributions_postales[0].code_commune_insee).toBe("75107")
})
