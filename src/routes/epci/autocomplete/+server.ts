import {
  type Audit,
  auditFunction,
  auditSetNullish,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"
import { json } from "@sveltejs/kit"
import fs from "fs-extra"
import MiniSearch from "minisearch"

import {
  auditQueryOptionsArray,
  auditQuerySingleton,
} from "$lib/auditors/queries"
import type { EpciAutocompletion } from "$lib/autocompletions"
import { communeByCode } from "$lib/communes"
import { epciByCode } from "$lib/epci"
import { simplify } from "$lib/strings"
import type { EpciSuggestion } from "$lib/suggestions"

import type { RequestHandler } from "./$types"

const epciAutocompletions = (await fs.readJson(
  "static/epci_autocompletions.json",
)) as EpciAutocompletion[]

const miniSearch = new MiniSearch({
  fields: [
    "autocompletion1",
    "autocompletion2",
    "autocompletion3",
    "autocompletion4",
    "autocompletion6",
    "autocompletion12",
    "code",
  ],
  idField: "index",
  searchOptions: {
    boost: {
      autocompletion1: 1,
      autocompletion2: 1.3,
      autocompletion3: 1.6,
      autocompletion4: 1.6,
      autocompletion6: 2,
      autocompletion12: 4,
    },
    fuzzy: 0.2,
    prefix: true,
    weights: {
      fuzzy: 0.5,
      prefix: 0.75,
    },
  },
  storeFields: [
    "autocompletion1",
    "autocompletion2",
    "autocompletion3",
    "autocompletion4",
    "autocompletion6",
    "autocompletion12",
    "code",
    "libelle",
    "role",
  ],
})
miniSearch.addAll(epciAutocompletions)

function auditQuery(audit: Audit, query: URLSearchParams): [unknown, unknown] {
  if (query == null) {
    return [query, null]
  }
  if (!(query instanceof URLSearchParams)) {
    return audit.unexpectedType(query, "URLSearchParams")
  }

  const data: { [key: string]: unknown } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key] as string[] | undefined
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "field",
    true,
    errors,
    remainingKeys,
    auditQueryOptionsArray(["communes", "epci"]),
  )
  audit.attribute(
    data,
    "q",
    true,
    errors,
    remainingKeys,
    auditQuerySingleton(
      auditTrimString,
      auditFunction((value: string) => simplify(value)),
    ),
  )

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export const GET: RequestHandler = ({ url }) => {
  const [query, queryError] = auditQuery(cleanAudit, url.searchParams) as [
    { field?: Array<"communes" | "epci">; q?: string },
    unknown,
  ]
  if (queryError !== null) {
    console.error(
      `Error in ${url.pathname} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
    return json({
      error: {
        query: queryError,
      },
    })
  }
  const { field: fields, q: term } = query

  let suggestions: EpciSuggestion[] = []
  const encounteredCodes = new Set<string>()
  for (const autocompletion of miniSearch.search(term ?? "")) {
    if (!encounteredCodes.has(autocompletion.code)) {
      encounteredCodes.add(autocompletion.code)
      suggestions.push({
        autocompletion:
          autocompletion.autocompletion1 ??
          autocompletion.autocompletion2 ??
          autocompletion.autocompletion3 ??
          autocompletion.autocompletion4 ??
          autocompletion.autocompletion6 ??
          autocompletion.autocompletion12,
        code: autocompletion.code,
        libelle: autocompletion.libelle,
        // match: autocompletion.match,
        role: autocompletion.role,
        score: autocompletion.score,
        // terms: autocompletion.terms,
      })
      if (suggestions.length >= 10) {
        break
      }
    }
  }

  if (fields?.includes("communes")) {
    suggestions = suggestions.map((suggestion) => ({
      ...suggestion,
      communes: epciByCode[suggestion.code].COM.map(
        (communeCode) => communeByCode[communeCode],
      ),
    }))
  }

  if (fields?.includes("epci")) {
    suggestions = suggestions.map((suggestion) => ({
      ...suggestion,
      epci: epciByCode[suggestion.code],
    }))
  }

  return json({
    suggestions,
  })
}
