import { type Audit, auditSetNullish, cleanAudit } from "@auditors/core"
import { json } from "@sveltejs/kit"

import { auditQueryOptionsArray } from "$lib/auditors/queries"
import type {
  Commune,
  CommuneCollectiviteOutreMer,
} from "$lib/code_officiel_geographique"
import { communeByCode } from "$lib/communes"
import {
  distributionsPostalesByCode,
  type DistributionPostale,
} from "$lib/distributions_postales"

import type { RequestHandler } from "./$types"

function auditQuery(audit: Audit, query: URLSearchParams): [unknown, unknown] {
  if (query == null) {
    return [query, null]
  }
  if (!(query instanceof URLSearchParams)) {
    return audit.unexpectedType(query, "URLSearchParams")
  }

  const data: { [key: string]: unknown } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key] as string[] | undefined
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "field",
    true,
    errors,
    remainingKeys,
    auditQueryOptionsArray(["distributions_postales"]),
  )

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export const GET: RequestHandler = ({ params, url }) => {
  const code = params.code.toUpperCase()
  const commune = communeByCode[code]
  if (commune === undefined) {
    return json({
      error: {
        code: "Invalid commune INSEE code",
      },
    })
  }

  const [query, queryError] = auditQuery(cleanAudit, url.searchParams) as [
    { field?: Array<"distributions_postales">; q?: string },
    unknown,
  ]
  if (queryError !== null) {
    console.error(
      `Error in ${url.pathname} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
    return json({
      error: {
        query: queryError,
      },
    })
  }
  const { field: fields } = query

  const body: {
    commune: Commune | CommuneCollectiviteOutreMer
    distributions_postales?: DistributionPostale[]
  } = {
    commune,
  }

  if (fields?.includes("distributions_postales")) {
    const distributionsPostales = distributionsPostalesByCode[code]
    if (distributionsPostales !== undefined) {
      body.distributions_postales = distributionsPostales
    }
  }

  return json(body)
}
