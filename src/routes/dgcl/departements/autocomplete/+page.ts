import type { DepartementSuggestion } from "$lib/suggestions"

import type { PageLoad } from "./$types"

export const load: PageLoad = async ({ fetch, url }) => {
  const response = await fetch(url, { headers: { Accept: "application/json" } })
  if (!response.ok) {
    const text = await response.text()
    console.error(
      `Error in ${url.pathname} while calling ${url}:\n${response.status} ${response.statusText}\n\n${text}`,
    )
    return {
      error: { fetch: `${response.status} ${response.statusText}\n\n${text}` },
    }
  }
  return (await response.json()) as {
    suggestions?: DepartementSuggestion[]
    error?: unknown
  }
}
