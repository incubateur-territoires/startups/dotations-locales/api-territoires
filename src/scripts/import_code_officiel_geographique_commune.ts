import fs from "fs-extra"
import Papa from "papaparse"

import type { Commune, TypeNomEnClair } from "$lib/code_officiel_geographique"

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const communes = ((await parseCsvFile("data/commune.csv")) as Commune[]).map(
  (commune) => {
    commune.TNCC = parseInt(commune.TNCC as unknown as string) as TypeNomEnClair
    for (const [key, value] of Object.entries(commune)) {
      if (value === "") {
        delete (commune as unknown as { [key: string]: unknown })[key]
      }
    }
    if (commune.COMPARENT === "") {
      delete commune.COMPARENT
    }
    return commune
  },
)
await fs.writeJson("static/commune.json", communes, {
  spaces: 2,
})
