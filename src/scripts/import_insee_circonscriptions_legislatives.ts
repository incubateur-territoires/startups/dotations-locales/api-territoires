// **Ne pas utiliser ce script** :
// Il manque dans le fichier les collectivités d'outre-mer (COM) (les départements et régions d'outre-mer (DROM) sont présents).

import assert from "assert"
import fs from "fs-extra"
import XLSX from "xlsx"

import {
  type CirconscriptionLegislative,
  type CirconscriptionLegislativeRow,
  departementCodeFromCirconscriptionLegislativeCode,
} from "$lib/circonscriptions_legislatives"
import { libelleWithCharniereFromTnccAndNccenr } from "$lib/code_officiel_geographique"
import { departementByCode } from "$lib/departements"

const tableKeys = [
  "DEP",
  "libdep",
  "REG",
  "libreg",
  "COMMUNE_RESID",
  "LIBcom",
  "circo",
  "type_com",
]
const sheetNames = ["Lisez_moi", "table"]

const workbook = XLSX.readFile("data/circo_composition.xlsx")
assert.equal(workbook.SheetNames.length, sheetNames.length)
assert(
  workbook.SheetNames.every(
    (sheetName, index) => sheetName === sheetNames[index],
  ),
)

const tableSheet = workbook.Sheets.table
const circosRows = XLSX.utils.sheet_to_json(
  tableSheet,
) as CirconscriptionLegislativeRow[]
const circoByCode: { [code: string]: CirconscriptionLegislative } = {}
for (const circoRow of circosRows) {
  if (circoRow.circo === undefined) {
    // Cas des villes sans habitants : villages français détruits durant
    // la Première Guerre mondiale
    continue
  }
  const keys = Object.keys(circoRow)
  assert.equal(keys.length, tableKeys.length)
  assert(keys.every((key, index) => key === tableKeys[index]))

  let circo = circoByCode[circoRow.circo]
  if (circo === undefined) {
    const circoIndex = parseInt(circoRow.circo.slice(3))
    const departementCode = departementCodeFromCirconscriptionLegislativeCode(
      circoRow.circo,
    )
    const departement = departementByCode[departementCode]
    circo = circoByCode[circoRow.circo] = {
      code: circoRow.circo,
      codes_communes: [],
      libelle: `${circoIndex}${
        circoIndex === 1 ? "ère" : "ème"
      } circonscription ${libelleWithCharniereFromTnccAndNccenr(
        departement.TNCC,
        departement.NCCENR,
      )}`,
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const communesCode = circo.codes_communes!
  // Handle special cases of communes with arrondissements
  // Cf:
  // * https://fr.wikipedia.org/wiki/Liste_des_circonscriptions_l%C3%A9gislatives_des_Bouches-du-Rh%C3%B4ne
  // * https://www.lyoncapitale.fr/actualite/grand-lyon-trouvez-la-circonscription-legislative-de-votre-ville-avec-notre-carte
  // * https://fr.wikipedia.org/wiki/Liste_des_circonscriptions_l%C3%A9gislatives_de_Paris
  switch (circoRow.circo) {
    // Marseille
    case "13001":
      // 1re circonscription : 10e arrondissement, 11e arrondissement, 12e arrondissement
      communesCode.push("13210", "13211", "13212")
      break
    case "13002":
      // 2e circonscription : 7e et 8e arrondissements.
      communesCode.push("13207", "13208")
      break
    case "13003":
      // 3e circonscription : 12e arrondissement, 13e arrondissement, 14e arrondissement
      communesCode.push("13212", "13213", "13214")
      break
    case "13004":
      // 4e circonscription : 1er, 2e et 3e arrondissements, 5e arrondissement, 6e arrondissement
      communesCode.push("13201", "13202", "13203", "13205", "13206")
      break
    case "13005":
      // 5e circonscription : 4e arrondissement, 5e arrondissement, 6e arrondissement
      communesCode.push("13204", "13205", "13206")
      break
    case "13006":
      // 6e circonscription : 9e arrondissement, 10e arrondissement
      communesCode.push("13209", "13210")
      break
    case "13007":
      // 7e circonscription : 14e arrondissement, 15e et 16e arrondissements.
      communesCode.push("13214", "13215", "13216")
      break

    // Lyon
    case "69001":
      // 1re circonscription : 2e arrondissement, 5e arrondissement, 7e arrondissement, 8e arrondissement, 9e arrondissement
      communesCode.push("69382", "69385", "69387", "69388", "69389")
      break
    case "69002":
      // 2e circonscription : 1er arrondissement, 2e arrondissement, 4e arrondissement, 9e arrondissement
      communesCode.push("69381", "69382", "69384", "69389")
      break
    case "69003":
      // 3e circonscription : 3e arrondissement, 7e arrondissement, 8e arrondissement
      communesCode.push("69383", "69387", "69388")
      break
    case "69004":
      // 4e circonscription : 3e arrondissement, 6e arrondissement, 8e arrondissement
      communesCode.push("69383", "69386", "69388")
      break

    // Paris
    case "75001":
      // 1re circonscription : 1er, 2e et 8e arrondissements, partie du 9e arrondissement
      communesCode.push("75101", "75102", "75108", "75109")
      break
    case "75002":
      // 2e circonscription : 5e arrondissement, partie du 6e arrondissement, partie du 7e arrondissement
      communesCode.push("75105", "75106")
      break
    case "75003":
      // 3e circonscription : partie du 17e arrondissement, partie du 18e arrondissement
      communesCode.push("75117", "75118")
      break
    case "75004":
      // 4e circonscription : partie du 16e arrondissement, partie du 17e arrondissement
      communesCode.push("75116", "75117")
      break
    case "75005":
      // 5e circonscription : 3e et 10e arrondissements
      communesCode.push("75103", "75110")
      break
    case "75006":
      // 6e circonscription : partie du 11e arrondissement, partie du 20e arrondissement
      communesCode.push("75111", "75120")
      break
    case "75007":
      // 7e circonscription : 4e arrondissement, partie du 11e arrondissement, partie du 12e arrondissement
      communesCode.push("75104", "75111", "75112")
      break
    case "75008":
      // 8e circonscription : partie du 12e arrondissement, partie du 20e arrondissement
      communesCode.push("75112", "75120")
      break
    case "75009":
      // 9e circonscription : partie du 13e arrondissement
      communesCode.push("75113")
      break
    case "75010":
      // 10e circonscription : partie du 13e arrondissement, partie du 14e arrondissement
      communesCode.push("75113", "75114")
      break
    case "75011":
      // 11e circonscription : partie du 6e arrondissement, partie du 14e arrondissement
      communesCode.push("75106", "75114")
      break
    case "75012":
      // 12e circonscription : partie du 7e arrondissement, partie du 15e arrondissement
      communesCode.push("75107", "75115")
      break
    case "75013":
      // 13e circonscription : partie du 15e arrondissement
      communesCode.push("75115")
      break
    case "75014":
      // 14e circonscription : partie du 16e arrondissement
      communesCode.push("75116")
      break
    case "75015":
      // 15e circonscription : partie du 20e arrondissement
      communesCode.push("75120")
      break
    case "75016":
      // 16e circonscription : partie du 19e arrondissement
      communesCode.push("75119")
      break
    case "75017":
      // 17e circonscription : partie du 18e arrondissement, partie du 19e arrondissement
      communesCode.push("75118", "75119")
      break
    case "75018":
      // 18e circonscription : partie du 9e arrondissement, partie du 18e arrondissement
      communesCode.push("75109", "75118")
      break
    default:
      communesCode.push(circoRow.COMMUNE_RESID)
  }
}

// Add libelles to circonscriptions.
for (const [circoCode, circo] of Object.entries(circoByCode)) {
  const circoIndex = parseInt(circoCode.slice(3))
  const departementCode =
    departementCodeFromCirconscriptionLegislativeCode(circoCode)
  const departement = departementByCode[departementCode]
  const departementLibelleWithCharniere = libelleWithCharniereFromTnccAndNccenr(
    departement.TNCC,
    departement.NCCENR,
  )
  const singleCircoInDepartement =
    circoIndex === 1 &&
    Object.keys(circoByCode).filter((code) => code.startsWith(departementCode))
      .length === 1
  circo.libelle = singleCircoInDepartement
    ? `Circonscription ${departementLibelleWithCharniere}`
    : `${circoIndex}${
        circoIndex === 1 ? "ère" : "ème"
      } circonscription ${departementLibelleWithCharniere}`
}

await fs.writeJson(
  "static/circonscriptions_legislatives.json",
  Object.entries(circoByCode)
    .sort(([code1], [code2]) => code1.localeCompare(code2))
    .map(([, circo]) => circo),
  {
    spaces: 2,
  },
)
