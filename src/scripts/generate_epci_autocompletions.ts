import fs from "fs-extra"

import type {
  CommuneAutocompletion,
  EpciAutocompletion,
} from "$lib/autocompletions"
import { epciByCode, type Epci } from "$lib/epci"
import { simplify } from "$lib/strings"

const communesAutocompletion = (await fs.readJson(
  "static/communes_autocompletions.json",
)) as CommuneAutocompletion[]
const epciEntries = (await fs.readJson("static/epci.json")) as Epci[]
const epciAutocompletions: EpciAutocompletion[] = []
const epciCodeByCommuneCode: { [key: string]: string } = {}
let index = 0
for (const epci of epciEntries) {
  epciAutocompletions.push({
    autocompletion12: simplify(epci.LIBEPCI),
    code: epci.EPCI,
    index: index++,
    libelle: epci.LIBEPCI,
  })
  for (const communeCode of epci.COM) {
    epciCodeByCommuneCode[communeCode] = epci.EPCI
  }
}

for (const communeAutocompletion of communesAutocompletion) {
  const epciCode = epciCodeByCommuneCode[communeAutocompletion.code]
  if (epciCode !== undefined) {
    const epci = epciByCode[epciCode]
    epciAutocompletions.push({
      ...communeAutocompletion,
      code: epciCode,
      index: index++,
      libelle: epci.LIBEPCI,
      role: communeAutocompletion.role ?? "commune",
    })
  }
}

await fs.writeJson("static/epci_autocompletions.json", epciAutocompletions, {
  spaces: 2,
})
