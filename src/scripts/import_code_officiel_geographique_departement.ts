import fs from "fs-extra"
import Papa from "papaparse"

import type {
  Departement,
  TypeNomEnClair,
} from "$lib/code_officiel_geographique"

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const departements = (
  (await parseCsvFile("data/departement.csv")) as Departement[]
).map((departement) => {
  departement.TNCC = parseInt(
    departement.TNCC as unknown as string,
  ) as TypeNomEnClair
  return departement
})
await fs.writeJson("static/departement.json", departements, {
  spaces: 2,
})
