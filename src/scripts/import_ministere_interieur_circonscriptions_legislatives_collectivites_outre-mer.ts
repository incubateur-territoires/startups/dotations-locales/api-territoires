import assert from "assert"
import fs from "fs-extra"
import XLSX from "xlsx"

import {
  type CirconscriptionLegislative,
  type CirconscriptionLegislativeMinistereInterieur,
  departementCodeFromCirconscriptionLegislativeCode,
} from "$lib/circonscriptions_legislatives"
import {
  type CollectiviteOutreMer,
  libelleWithCharniereFromTnccAndNccenr,
  type EvenementCommune,
} from "$lib/code_officiel_geographique"
import { communeByCode } from "$lib/communes"
import { departementByCode } from "$lib/departements"

export function communeCodeFromMinistereInterieurCodes(
  departementCode: number | string,
  communeCode: number,
): string {
  if (typeof departementCode === "number") {
    // Département métropolitain
    return (
      departementCode.toString().padStart(2, "0") +
      communeCode.toString().padStart(3, "0")
    )
  }
  switch (departementCode) {
    case "2A":
    case "2B":
      // Corse
      return (
        departementCode.toString().padStart(2, "0") +
        communeCode.toString().padStart(3, "0")
      )

    case "ZA":
    // Guadeloupe
    case "ZB": // eslint-disable-line no-fallthrough
    // Martinique
    case "ZC": // eslint-disable-line no-fallthrough
    // Guyane
    case "ZD": // eslint-disable-line no-fallthrough
    // La Réunion
    case "ZS": // eslint-disable-line no-fallthrough
    // Saint-Pierre-et-Miquelon
    case "ZX": // eslint-disable-line no-fallthrough
      // Saint-Barthélemy
      // Saint-Martin
      return "97" + communeCode.toString().padStart(3, "0")

    case "ZN":
      // Nouvelle-Calédonie
      return "98" + communeCode.toString().padStart(3, "0")

    case "ZM":
      // Mayotte
      // In 2011, Mayotte codes changed from 985XX to 976XX.
      return "97" + (communeCode + 100).toString().padStart(3, "0")

    case "ZP":
      // Polynésie française
      return "987" + communeCode.toString().padStart(2, "0")

    case "ZW":
      // Wallis-et-Futuna
      return "986" + communeCode.toString().padStart(2, "0")

    case "ZZ":
      // Français établis hors de France
      return "99"

    default:
      throw new Error(`Unexpected departement code: ${departementCode}`)
  }
}

const tableKeys = [
  "CODE DPT",
  "NOM DPT",
  "CODE COMMUNE",
  "NOM COMMUNE",
  "CODE CIRC LEGISLATIVE",
  "CODE CANTON",
  "NOM CANTON",
]
const sheetNames = ["PR17_Découpage"]

const workbook = XLSX.readFile("data/circonscriptions_legislatives.xlsx")
assert.equal(workbook.SheetNames.length, sheetNames.length)
assert(
  workbook.SheetNames.every(
    (sheetName, index) => sheetName === sheetNames[index],
  ),
)

const sheet = workbook.Sheets["PR17_Découpage"]
const circosRows = XLSX.utils.sheet_to_json(
  sheet,
) as CirconscriptionLegislativeMinistereInterieur[]

const collectivitesOutreMer = (await fs.readJson(
  "static/comer.json",
)) as CollectiviteOutreMer[]
const collectivitesOutreMerCode = new Set(
  collectivitesOutreMer.map((collectivite) => collectivite.COMER),
)

const evenementsCommunes = (await fs.readJson(
  "static/mvtcommune.json",
)) as EvenementCommune[]
const latestEvenementByCommuneCode: { [code: string]: EvenementCommune } = {}
for (const evenement of evenementsCommunes) {
  // Ignore events that don't change Insee code.
  if (evenement.COM_AP === evenement.COM_AV) {
    continue
  }

  let latestEvenement = latestEvenementByCommuneCode[evenement.COM_AV]
  if (latestEvenement === undefined) {
    latestEvenement = latestEvenementByCommuneCode[evenement.COM_AV] = evenement
  }
}

const circonscriptionsLegislativesInsee = (await fs.readJson(
  "static/circonscriptions_legislatives.json",
)) as CirconscriptionLegislative[]
const circoByCode: { [code: string]: CirconscriptionLegislative } =
  Object.fromEntries(
    circonscriptionsLegislativesInsee.map((circo) => [circo.code, circo]),
  )

for (const circoRow of circosRows) {
  const keys = Object.keys(circoRow)
  assert.equal(keys.length, tableKeys.length)
  assert(keys.every((key, index) => key === tableKeys[index]))

  let communeCode = communeCodeFromMinistereInterieurCodes(
    circoRow["CODE DPT"],
    circoRow["CODE COMMUNE"],
  )
  assert.notStrictEqual(communeCode, undefined)
  if (communeCode === "99") {
    // Ignore Français établis hors de France:
    // They are handled separately.
    continue
  }

  // Replace commune code whith the latest code in Insee code officiel géographique.
  let latestEvenementDate = "2015-01-01"
  for (
    let latestEvenement = latestEvenementByCommuneCode[communeCode];
    latestEvenement !== undefined &&
    latestEvenement.DATE_EFF > latestEvenementDate;
    latestEvenement = latestEvenementByCommuneCode[communeCode]
  ) {
    // console.log(JSON.stringify(latestEvenement))
    latestEvenementDate = latestEvenement.DATE_EFF
    communeCode = latestEvenement.COM_AP
  }
  if (communeCode !== "98601") {
    // 98601 = pseudo commune of Wallis-et-Futuna
    assert.notStrictEqual(
      communeByCode[communeCode],
      undefined,
      `Unknown commune code ${communeCode} for ${JSON.stringify(circoRow)}`,
    )
  }

  let circoCode =
    communeCode.startsWith("97") || communeCode.startsWith("98")
      ? communeCode.slice(0, 3) +
        circoRow["CODE CIRC LEGISLATIVE"].toString().padStart(2, "0")
      : communeCode.slice(0, 2) +
        circoRow["CODE CIRC LEGISLATIVE"].toString().padStart(3, "0")
  // Saint-Barthélémy (97701) & Saint-Martin (97801) are merged into the same circonscription.
  if (circoCode === "97801") {
    circoCode = "97701"
  }

  const departementCode =
    departementCodeFromCirconscriptionLegislativeCode(circoCode)
  if (
    departementCode !== "976" &&
    !collectivitesOutreMerCode.has(departementCode)
  ) {
    // Handle only Mayotte & collectivités d'outre-mer.
    continue
  }

  let circo = circoByCode[circoCode]
  if (circo === undefined) {
    circo = circoByCode[circoCode] = {
      code: circoCode,
      codes_communes: [],
      libelle: "", // Set later
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const communesCode = circo.codes_communes!
  if (communeCode === "98601") {
    // 98601 = pseudo commune of Wallis-et-Futuna
    // => Add all communes of collectivité d'outrer-mer.
    const collectiviteOutreMerCode = communeCode.slice(0, 3)
    const collectiviteOutreMerCommunesCode = Object.keys(communeByCode).filter(
      (code) => code.startsWith(collectiviteOutreMerCode),
    )
    communesCode.push(...collectiviteOutreMerCommunesCode)
  } else if (!communesCode.includes(communeCode)) {
    communesCode.push(communeCode)
  }
}

// Add "circonscriptions des Français établis hors de France".
for (let circoIndex = 1; circoIndex < 12; circoIndex++) {
  const circoCode = "99" + circoIndex.toString().padStart(3, "0")
  circoByCode[circoCode] = {
    code: circoCode,
    libelle: "", // Set later
  }
}

// Add libelles to circonscriptions and sort their commmunes.
for (const [circoCode, circo] of Object.entries(circoByCode)) {
  const circoIndex = parseInt(circoCode.slice(3))
  const departementCode =
    departementCodeFromCirconscriptionLegislativeCode(circoCode)
  const departement = departementByCode[departementCode]
  const pseudoDepartementLibelleWithCharniere = circoCode.startsWith("977")
    ? "de Saint-Barthélemy et Saint-Martin"
    : circoCode.startsWith("99")
    ? "des Français établis hors de France"
    : libelleWithCharniereFromTnccAndNccenr(
        departement.TNCC,
        departement.NCCENR,
      )
  const singleCircoInDepartement =
    circoIndex === 1 &&
    Object.keys(circoByCode).filter((code) => code.startsWith(departementCode))
      .length === 1
  circo.libelle = singleCircoInDepartement
    ? `Circonscription ${pseudoDepartementLibelleWithCharniere}`
    : `${circoIndex}${
        circoIndex === 1 ? "ère" : "ème"
      } circonscription ${pseudoDepartementLibelleWithCharniere}`

  if (circo.codes_communes !== undefined) {
    circo.codes_communes.sort()
  }
}

await fs.writeJson(
  "static/circonscriptions_legislatives.json",
  Object.entries(circoByCode)
    .sort(([code1], [code2]) => code1.localeCompare(code2))
    .map(([, circo]) => circo),
  {
    spaces: 2,
  },
)
