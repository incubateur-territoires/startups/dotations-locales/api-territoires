import fs from "fs-extra"
import Papa from "papaparse"

import type {
  CommuneCollectiviteOutreMer,
  TypeNomEnClair,
} from "$lib/code_officiel_geographique"

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const communes = (
  (await parseCsvFile("data/com_comer.csv")) as CommuneCollectiviteOutreMer[]
).map((commune) => {
  commune.TNCC = parseInt(commune.TNCC as unknown as string) as TypeNomEnClair
  return commune
})
await fs.writeJson("static/com_comer.json", communes, {
  spaces: 2,
})
