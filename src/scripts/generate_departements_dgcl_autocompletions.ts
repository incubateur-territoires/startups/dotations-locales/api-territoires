import fs from "fs-extra"

import type {
  CommuneAutocompletion,
  DepartementAutocompletion,
} from "$lib/autocompletions"
import type { CommuneDgcl, DepartementDgcl } from "$lib/dgcl"
import { simplify } from "$lib/strings"

const communesAutocompletion = (await fs.readJson(
  "static/communes_dgcl_autocompletions.json",
)) as CommuneAutocompletion[]
const communeDgclByCode: { [code: string]: CommuneDgcl } = Object.fromEntries(
  (await fs.readJson("static/communes_dgcl.json")).map(
    (communeDgcl: CommuneDgcl) => [
      communeDgcl["Code INSEE de la commune"],
      communeDgcl,
    ],
  ),
)
const departementsDgclEntries = (await fs.readJson(
  "static/departements_dgcl.json",
)) as DepartementDgcl[]
const departementDgclByCode = Object.fromEntries(
  departementsDgclEntries.map((departementDgcl) => [
    departementDgcl["Numéro département"],
    departementDgcl,
  ]),
)
const departementsAutocompletions: DepartementAutocompletion[] = []
let index = 0
for (const departementDgcl of departementsDgclEntries) {
  departementsAutocompletions.push({
    autocompletion12: [
      departementDgcl["Numéro département"],
      simplify(departementDgcl["Nom département"]),
    ]
      .filter(Boolean)
      .join(" "),
    code: departementDgcl["Numéro département"],
    index: index++,
    libelle: departementDgcl["Nom département"],
  })
}

for (const communeAutocompletion of communesAutocompletion) {
  const communeDgcl = communeDgclByCode[communeAutocompletion.code]
  const departementCode = communeDgcl["Code département de la commune"]
  if (departementCode !== undefined) {
    const departementDgcl = departementDgclByCode[departementCode]
    if (departementDgcl === undefined) {
      if (!departementCode.startsWith("98")) {
        console.warn(`Département DGCL "${departementCode}" not found`)
      }
      continue
    }
    departementsAutocompletions.push({
      ...communeAutocompletion,
      code: departementCode,
      index: index++,
      libelle: departementDgcl["Nom département"],
      role: communeAutocompletion.role ?? "commune",
    })
  }
}

await fs.writeJson(
  "static/departements_dgcl_autocompletions.json",
  departementsAutocompletions,
  {
    spaces: 2,
  },
)
