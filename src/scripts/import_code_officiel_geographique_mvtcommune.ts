import fs from "fs-extra"
import Papa from "papaparse"

import type {
  EvenementCommune,
  TypeEvenementCommune,
  TypeNomEnClair,
} from "$lib/code_officiel_geographique"

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const evenementsCommunes = (
  (await parseCsvFile("data/mvtcommune.csv")) as EvenementCommune[]
).map((evenementCommune) => {
  evenementCommune.MOD = parseInt(
    evenementCommune.MOD as unknown as string,
  ) as TypeEvenementCommune
  evenementCommune.TNCC_AV = parseInt(
    evenementCommune.TNCC_AV as unknown as string,
  ) as TypeNomEnClair
  evenementCommune.TNCC_AP = parseInt(
    evenementCommune.TNCC_AP as unknown as string,
  ) as TypeNomEnClair
  return evenementCommune
})
await fs.writeJson("static/mvtcommune.json", evenementsCommunes, {
  spaces: 2,
})
