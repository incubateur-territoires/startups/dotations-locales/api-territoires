import fs from "fs-extra"

import type { CommuneAutocompletion } from "$lib/autocompletions"
import {
  type Commune,
  type CommuneCollectiviteOutreMer,
  libelleSimplifieFromTnccAndNcc,
} from "$lib/code_officiel_geographique"
import type { CommuneDgcl } from "$lib/dgcl"
import type { DistributionPostale } from "$lib/distributions_postales"
import { simplify } from "$lib/strings"

interface LibelleSimplifie {
  libelle: string
  role?: "distribution postale"
}

function* iterAcceptedCodesPostaux({
  code,
  codes_postaux,
}: {
  code: string
  codes_postaux: string[]
}): Generator<
  {
    name: string | null
    weight: number
  },
  void,
  unknown
> {
  yield { name: null, weight: 1 }
  yield { name: code, weight: 2 }
  for (const codePostal of codes_postaux) {
    yield { name: codePostal, weight: 3 }
  }
}

function* iterAcceptedLibelles({
  libelles_simplifies,
  lignes_5,
}: {
  libelles_simplifies: LibelleSimplifie[]
  lignes_5?: string[]
}): Generator<
  {
    name: string | null
    role?: "distribution postale" | "lieu-dit"
    weight: number
  },
  void,
  unknown
> {
  // yield { alternative: true, name: null }
  const encountered = new Set<string>()
  for (const { libelle, role } of libelles_simplifies) {
    if (!encountered.has(libelle)) {
      encountered.add(libelle)
      yield { name: libelle, role, weight: 2 }
    }
    const libelleWithoutLeadingZeros = libelle.replace(/\d+/, (number) =>
      parseInt(number).toString(),
    )
    if (!encountered.has(libelleWithoutLeadingZeros)) {
      encountered.add(libelleWithoutLeadingZeros)
      yield { name: libelleWithoutLeadingZeros, role, weight: 2 }
    }
  }
  for (const libelle of lignes_5 ?? []) {
    if (!encountered.has(libelle)) {
      encountered.add(libelle)
      yield { name: libelle, role: "lieu-dit", weight: 1 }
    }
  }
}

const distributionsPostales = (await fs.readJson(
  "static/distributions_postales.json",
)) as DistributionPostale[]
const communeByCode: { [code: string]: Commune | CommuneCollectiviteOutreMer } =
  {
    ...Object.fromEntries(
      (await fs.readJson("static/commune.json")).map((commune: Commune) => [
        commune.COM,
        commune,
      ]),
    ),
    ...Object.fromEntries(
      (await fs.readJson("static/com_comer.json")).map(
        (commune: CommuneCollectiviteOutreMer) => [commune.COM_COMER, commune],
      ),
    ),
  }
const communeDgclByCode: { [code: string]: CommuneDgcl } = Object.fromEntries(
  (await fs.readJson("static/communes_dgcl.json")).map(
    (communeDgcl: CommuneDgcl) => [
      communeDgcl["Code INSEE de la commune"],
      communeDgcl,
    ],
  ),
)

const communeAutocompletionDataByCode: {
  [code: string]: {
    code: string
    codes_postaux: string[]
    libelles_simplifies: LibelleSimplifie[]
    lignes_5?: string[]
  }
} = {}
for (const distributionPostale of distributionsPostales) {
  if (distributionPostale.nom_de_la_commune === "MONACO") {
    continue
  }
  const communeAutocompletionData =
    communeAutocompletionDataByCode[distributionPostale.code_commune_insee]
  if (communeAutocompletionData === undefined) {
    const commune = communeByCode[distributionPostale.code_commune_insee]
    let communeDgcl = communeDgclByCode[distributionPostale.code_commune_insee]
    if (communeDgcl === undefined) {
      if (commune !== undefined) {
        for (let parent = commune; communeDgcl === undefined; ) {
          const parentCode = (parent as Commune).COMPARENT
          if (
            parentCode === undefined ||
            parentCode ===
              ((parent as Commune).COM ??
                (parent as CommuneCollectiviteOutreMer).COM_COMER)
          ) {
            break
          }
          parent = communeByCode[parentCode]
          communeDgcl = communeDgclByCode[parentCode]
        }
      }
      if (communeDgcl === undefined) {
        console.warn(
          `Commune with INSEE code ${distributionPostale.code_commune_insee} present in file La Poste hexasmal but missing from DGCL.`,
        )
        continue
      }
    }

    const libellesSimplifies: LibelleSimplifie[] = []
    if (
      !libellesSimplifies.some(
        ({ libelle }) => libelle === communeDgcl["Nom de la commune"],
      )
    ) {
      libellesSimplifies.push({
        libelle: simplify(communeDgcl["Nom de la commune"]),
      })
    }

    if (commune !== undefined) {
      const libelleSimplifieWithArticle = libelleSimplifieFromTnccAndNcc(
        commune.TNCC,
        commune.NCC,
      )
      if (
        !libellesSimplifies.some(
          ({ libelle }) => libelle === libelleSimplifieWithArticle,
        )
      ) {
        libellesSimplifies.push({ libelle: libelleSimplifieWithArticle })
      }
      if (!libellesSimplifies.some(({ libelle }) => libelle === commune.NCC)) {
        libellesSimplifies.push({ libelle: commune.NCC })
      }
    }

    if (
      !libellesSimplifies.some(
        ({ libelle }) => libelle === distributionPostale.nom_de_la_commune,
      )
    ) {
      libellesSimplifies.push({
        libelle: distributionPostale.nom_de_la_commune,
        role: "distribution postale",
      })
    }

    communeAutocompletionDataByCode[distributionPostale.code_commune_insee] = {
      code: communeDgcl["Code INSEE de la commune"],
      codes_postaux: [distributionPostale.code_postal],
      libelles_simplifies: libellesSimplifies,
      lignes_5:
        distributionPostale.ligne_5 === undefined
          ? undefined
          : [distributionPostale.ligne_5],
    }
  } else {
    if (
      !communeAutocompletionData.libelles_simplifies.some(
        ({ libelle }) => libelle === distributionPostale.nom_de_la_commune,
      )
    ) {
      communeAutocompletionData.libelles_simplifies.push({
        libelle: distributionPostale.nom_de_la_commune,
        role: "distribution postale",
      })
    }
    if (distributionPostale.ligne_5 !== undefined) {
      if (communeAutocompletionData.lignes_5 === undefined) {
        communeAutocompletionData.lignes_5 = []
      }
      if (
        !communeAutocompletionData.lignes_5.includes(
          distributionPostale.ligne_5,
        )
      ) {
        communeAutocompletionData.lignes_5.push(distributionPostale.ligne_5)
      }
    }
    if (
      !communeAutocompletionData.codes_postaux.includes(
        distributionPostale.code_postal,
      )
    ) {
      communeAutocompletionData.codes_postaux.push(
        distributionPostale.code_postal,
      )
    }
  }
}
const communesAutocompletion: CommuneAutocompletion[] = []
let index = 0
for (const communeAutocompletionData of Object.values(
  communeAutocompletionDataByCode,
)) {
  for (const {
    name: codePostal,
    weight: codePostalWeight,
  } of iterAcceptedCodesPostaux(communeAutocompletionData)) {
    for (const {
      name: libelle,
      role,
      weight: libelleWeight,
    } of iterAcceptedLibelles(communeAutocompletionData)) {
      // When "libelle" contains a digit (eg PARIS 7), accept an autocompletion
      // without postal code.
      if (codePostal !== null || (libelle !== null && /\d/.test(libelle))) {
        const autocompletion = [codePostal, libelle].filter(Boolean).join(" ")
        if (autocompletion !== "") {
          const communeDgcl = communeDgclByCode[communeAutocompletionData.code]
          const weight = codePostalWeight * libelleWeight
          communesAutocompletion.push({
            [`autocompletion${weight}`]: autocompletion,
            code: communeAutocompletionData.code,
            index: index++,
            libelle: communeDgcl["Nom de la commune"],
            role,
          })
        }
      }
    }
  }
}

await fs.writeJson(
  "static/communes_dgcl_autocompletions.json",
  communesAutocompletion,
  {
    spaces: 2,
  },
)
