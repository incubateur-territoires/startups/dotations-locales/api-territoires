import assert from "assert"
import fs from "fs-extra"
import XLSX from "xlsx"

import type { DepartementDgcl } from "$lib/dgcl"

const tableKeys = ["Numéro département", "Nom département", "Numéro région"]
const sheetNames = [
  "Valeurs moyennes",
  "Critères métropole",
  "Critères outre-mer",
]

const workbook = XLSX.readFile("data/criteres_repartition_departements.xls")
assert.equal(workbook.SheetNames.length, sheetNames.length)
assert(
  workbook.SheetNames.every(
    (sheetName, index) => sheetName === sheetNames[index],
  ),
)

const departementsDgcl: DepartementDgcl[] = []
for (const [sheetName, labelsIndex] of [
  ["Critères métropole", 3],
  ["Critères outre-mer", 5],
] as Array<[string, number]>) {
  const sheet = workbook.Sheets[sheetName]
  const rows = XLSX.utils.sheet_to_json(sheet, { header: 1 }) as Array<
    Array<string>
  >

  const labels = rows[labelsIndex]
  assert(tableKeys.every((tableKey, index) => tableKey === labels[index]))
  for (const row of rows.slice(4)) {
    departementsDgcl.push(
      Object.fromEntries(
        tableKeys.map((tableKey, index) => [tableKey, row[index]]),
      ) as unknown as DepartementDgcl,
    )
  }
}

await fs.writeJson("static/departements_dgcl.json", departementsDgcl, {
  spaces: 2,
})
