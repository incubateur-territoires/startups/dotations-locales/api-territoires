import fs from "fs-extra"

import type {
  CommuneAutocompletion,
  EpciAutocompletion,
} from "$lib/autocompletions"
import type { CommuneDgcl } from "$lib/dgcl"
import { simplify } from "$lib/strings"

const communesAutocompletion = (await fs.readJson(
  "static/communes_dgcl_autocompletions.json",
)) as CommuneAutocompletion[]
const communesDgclEntries = (await fs.readJson(
  "static/communes_dgcl.json",
)) as CommuneDgcl[]
const epciAutocompletions: EpciAutocompletion[] = []
const epciCodeByCommuneCode: { [key: string]: string } = {}
const epciDgclByCode: {
  [key: string]: { "Code SIREN de l'EPCI": string; "Nom de l'EPCI": string }
} = {}
let index = 0
for (const communeDgcl of communesDgclEntries) {
  const epciCode = communeDgcl["Code SIREN de l'EPCI"]
  if (epciCode === undefined) {
    continue
  }
  let epciDgcl = epciDgclByCode[epciCode]
  if (epciDgcl === undefined) {
    const epciName = communeDgcl["Nom de l'EPCI"] as string
    epciDgcl = epciDgclByCode[epciCode] = {
      "Code SIREN de l'EPCI": epciCode,
      "Nom de l'EPCI": epciName,
    }
    epciAutocompletions.push({
      autocompletion12: simplify(epciName),
      code: epciCode,
      index: index++,
      libelle: epciName,
    })
  }
  epciCodeByCommuneCode[communeDgcl["Code INSEE de la commune"]] = epciCode
}

for (const communeAutocompletion of communesAutocompletion) {
  const epciCode = epciCodeByCommuneCode[communeAutocompletion.code]
  if (epciCode !== undefined) {
    const epciDgcl = epciDgclByCode[epciCode]
    epciAutocompletions.push({
      ...communeAutocompletion,
      code: epciCode,
      index: index++,
      libelle: epciDgcl["Nom de l'EPCI"],
      role: communeAutocompletion.role ?? "commune",
    })
  }
}

await fs.writeJson(
  "static/epci_dgcl_autocompletions.json",
  epciAutocompletions,
  {
    spaces: 2,
  },
)
