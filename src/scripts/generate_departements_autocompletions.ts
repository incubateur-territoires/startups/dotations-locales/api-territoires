import fs from "fs-extra"

import type {
  CommuneAutocompletion,
  DepartementAutocompletion,
} from "$lib/autocompletions"
import type {
  CollectiviteOutreMer,
  Commune,
  CommuneCollectiviteOutreMer,
  Departement,
} from "$lib/code_officiel_geographique"
import { communeByCode } from "$lib/communes"
import { departementByCode } from "$lib/departements"

const communesAutocompletion = (await fs.readJson(
  "static/communes_autocompletions.json",
)) as CommuneAutocompletion[]
const departementsEntries = (await fs.readJson(
  "static/departement.json",
)) as Departement[]
const departementsAutocompletions: DepartementAutocompletion[] = []
let index = 0
for (const departement of departementsEntries) {
  departementsAutocompletions.push({
    autocompletion12: [departement.DEP, departement.NCC]
      .filter(Boolean)
      .join(" "),
    code: departement.DEP,
    index: index++,
    libelle: departement.LIBELLE,
  })
}

for (const communeAutocompletion of communesAutocompletion) {
  const commune = communeByCode[communeAutocompletion.code]
  let departementCode =
    (commune as Commune).DEP ?? (commune as CommuneCollectiviteOutreMer).COMER
  for (let parent = commune; departementCode === undefined; ) {
    const parentCode = (parent as Commune).COMPARENT
    if (
      parentCode === undefined ||
      parentCode ===
        ((parent as Commune).COM ??
          (parent as CommuneCollectiviteOutreMer).COM_COMER)
    ) {
      break
    }
    parent = communeByCode[parentCode]
    departementCode =
      (parent as Commune).DEP ?? (parent as CommuneCollectiviteOutreMer).COMER
  }
  if (departementCode !== undefined) {
    const departement = departementByCode[departementCode]
    departementsAutocompletions.push({
      ...communeAutocompletion,
      code:
        (departement as Departement).DEP ??
        (departement as CollectiviteOutreMer).COMER,
      index: index++,
      libelle: departement.LIBELLE,
      role: communeAutocompletion.role ?? "commune",
    })
  }
}

await fs.writeJson(
  "static/departements_autocompletions.json",
  departementsAutocompletions,
  {
    spaces: 2,
  },
)
