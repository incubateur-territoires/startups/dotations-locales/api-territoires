import fs from "fs-extra"

type Hexasmal = Array<{
  datasetid: string
  recordid: string
  fields: {
    nom_de_la_commune: string
    libelle_d_acheminement: string
    code_postal: string
    coordonnees_gps: [number, number]
    code_commune_insee: string
    ligne_5?: string
  }
  geometry: {
    type: "Point"
    coordinates: [number, number]
  }
  record_timestamp: string
}>

const hexasmal = (await fs.readJson("data/laposte_hexasmal.json")) as Hexasmal
const distributionsPostales = hexasmal.map(({ fields }) => fields)
await fs.writeJson(
  "static/distributions_postales.json",
  distributionsPostales,
  {
    spaces: 2,
  },
)
