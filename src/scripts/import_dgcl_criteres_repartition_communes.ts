import assert from "assert"
import fs from "fs-extra"
import XLSX from "xlsx"

import type { CommuneDgcl } from "$lib/dgcl"

const tableKeys = [
  "Code INSEE de la commune",
  "Nom de la commune",
  "Code département de la commune",
  "Code SIREN de l'EPCI",
  "Nom de l'EPCI",
]
const sheetNames = ["Valeurs moyennes", "Critères de répartition"]

const workbook = XLSX.readFile("data/criteres_repartition_communes.xls")
assert.equal(workbook.SheetNames.length, sheetNames.length)
assert(
  workbook.SheetNames.every(
    (sheetName, index) => sheetName === sheetNames[index],
  ),
)

const sheet = workbook.Sheets["Critères de répartition"]
const rows = XLSX.utils.sheet_to_json(sheet, { header: 1 }) as Array<
  Array<string>
>

const labels = rows[3]
assert(tableKeys.every((tableKey, index) => tableKey === labels[index]))
const communesDgcl = rows.slice(4).map((row) => {
  const communeDgcl = Object.fromEntries(
    tableKeys.map((tableKey, index) => [tableKey, row[index]]),
  ) as unknown as CommuneDgcl
  if (communeDgcl["Code SIREN de l'EPCI"] === "0") {
    delete communeDgcl["Code SIREN de l'EPCI"]
    delete communeDgcl["Nom de l'EPCI"]
  }
  return communeDgcl
})

await fs.writeJson("static/communes_dgcl.json", communesDgcl, {
  spaces: 2,
})
