import fs from "fs-extra"

import type {
  CirconscriptionLegislativeAutocompletion,
  CommuneAutocompletion,
} from "$lib/autocompletions"
import {
  type CirconscriptionLegislative,
  departementCodeFromCirconscriptionLegislativeCode,
  circonscriptionLegislativeByCode,
} from "$lib/circonscriptions_legislatives"
import { deputeByCirconscriptionLegislativeCode } from "$lib/deputes"
import { simplify } from "$lib/strings"

const communesAutocompletion = (await fs.readJson(
  "static/communes_autocompletions.json",
)) as CommuneAutocompletion[]
const circos = (await fs.readJson(
  "static/circonscriptions_legislatives.json",
)) as CirconscriptionLegislative[]

const circosAutocompletion: CirconscriptionLegislativeAutocompletion[] = []
let index = 0
const circosCodeByCommuneCode: { [key: string]: string[] } = {}
for (const circo of circos) {
  circosAutocompletion.push({
    autocompletion12: simplify(circo.libelle),
    code: circo.code,
    index: index++,
    libelle: circo.libelle,
  })

  const departementCode = departementCodeFromCirconscriptionLegislativeCode(
    circo.code,
  )
  if (departementCode.startsWith("99")) {
    circosAutocompletion.push({
      autocompletion12: simplify(circo.libelle).replace(
        "FRANCAIS ETABLIS HORS DE FRANCE",
        "FRANCAIS DE L ETRANGER",
      ),
      code: circo.code,
      index: index++,
      libelle: circo.libelle,
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const depute = deputeByCirconscriptionLegislativeCode[circo.code]
  // Sometimes a député is invalidated.
  // => Circonscription temporarily has no député.
  if (depute !== undefined) {
    const { nom, prenom } = depute.etatCivil.ident

    circosAutocompletion.push({
      autocompletion6: `${simplify(prenom)} ${simplify(nom)}`,
      code: circo.code,
      index: index++,
      libelle: circo.libelle,
      role: "député",
    })
  }

  for (const communeCode of circo.codes_communes ?? []) {
    let circosCode = circosCodeByCommuneCode[communeCode]
    if (circosCode === undefined) {
      circosCode = circosCodeByCommuneCode[communeCode] = []
    }
    circosCode.push(circo.code)
  }
}

for (const communeAutocompletion of communesAutocompletion) {
  const circosCode = circosCodeByCommuneCode[communeAutocompletion.code]
  if (circosCode !== undefined) {
    for (const circoCode of circosCode) {
      const circo = circonscriptionLegislativeByCode[circoCode]
      circosAutocompletion.push({
        ...communeAutocompletion,
        index: index++,
        code: circoCode,
        libelle: circo.libelle,
        role: communeAutocompletion.role ?? "commune",
      })
    }
  }
}

await fs.writeJson(
  "static/circonscriptions_legislatives_autocompletions.json",
  circosAutocompletion,
  {
    spaces: 2,
  },
)
