import fs from "fs-extra"

export interface DistributionPostale {
  nom_de_la_commune: string
  libelle_d_acheminement: string
  code_postal: string
  coordonnees_gps: [number, number] // latitude, longitude
  code_commune_insee: string
  /**
   * Ligne 5 dans l'écriture de l'adresse, utilisée notamment pour préciser l'ancienne commune ou le lieu-dit
   */
  ligne_5?: string
}

const distributionsPostales = (await fs.readJson(
  "static/distributions_postales.json",
)) as DistributionPostale[]
export const distributionsPostalesByCode: {
  [code: string]: DistributionPostale[]
} = {}
for (const codePostal of distributionsPostales) {
  let distributionsPostales =
    distributionsPostalesByCode[codePostal.code_commune_insee]
  if (distributionsPostales === undefined) {
    distributionsPostales = distributionsPostalesByCode[
      codePostal.code_commune_insee
    ] = []
  }
  distributionsPostales.push(codePostal)
}
