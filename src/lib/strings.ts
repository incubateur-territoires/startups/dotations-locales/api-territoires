import originalSlugify from "slug"

const slugifyCharmap = {
  ...originalSlugify.defaults.charmap,
  "'": " ",
  "@": " ",
  ".": " ",
}

export function simplify(string: string) {
  return slugify(string.replace(/-/g, " "), " ").toUpperCase()
}

export function slugify(string: string, replacement?: string | null) {
  const options: {
    charmap: { [character: string]: string }
    mode: string
    replacement: string
  } = {
    charmap: slugifyCharmap,
    mode: "rfc3986",
    replacement: "-",
  }
  if (replacement) {
    options.replacement = replacement
  }
  return originalSlugify(string, options)
}
