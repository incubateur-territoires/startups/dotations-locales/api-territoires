import {
  auditArray,
  auditChain,
  auditCleanArray,
  auditFunction,
  auditNullish,
  auditOptions,
  type Auditor,
  auditSetNullish,
  auditSwitch,
  auditTest,
  auditTrimString,
  auditUnique,
} from "@auditors/core"

export const auditQueryArray = auditChain(
  auditSwitch(
    auditNullish,
    [auditTrimString, auditFunction((value) => [value])],
    auditCleanArray(auditTrimString),
  ),
  auditSetNullish([]),
)

export function auditQueryOptionsArray(possibleValues: string[]): Auditor {
  return auditChain(
    auditQueryArray,
    auditArray(auditOptions(possibleValues)),
    auditUnique,
    auditSetNullish([]),
  )
}

export function auditQueryOptionsSet(possibleValues: string[]): Auditor {
  return auditChain(
    auditQueryOptionsArray(possibleValues),
    auditFunction((values) => new Set(values)),
    auditSetNullish(new Set()),
  )
}

export const auditQuerySet = auditChain(
  auditQueryArray,
  auditFunction((values) => new Set(values)),
  auditSetNullish(new Set()),
)

export function auditQuerySingleton(...auditors: Auditor[]) {
  return auditChain(
    auditArray(),
    auditTest(
      (values) => values.length <= 1,
      "Parameter must be present only once in query",
    ),
    auditFunction((value) => value[0]),
    ...auditors,
  )
}
