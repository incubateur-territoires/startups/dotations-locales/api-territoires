import fs from "fs-extra"

import {
  type Acteur,
  CodeTypeOrgane,
  type Mandat,
  TypeMandat,
} from "@tricoteuses/assemblee"

const now = new Date().toISOString()

export const deputeByCirconscriptionLegislativeCode = Object.fromEntries(
  ((await fs.readJson("static/deputes.json")) as Acteur[]).map((depute) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const mandatDeputeActif = mandatDeputeActifFromDepute(depute)!
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const lieu = mandatDeputeActif.election!.lieu
    const departementCode =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      lieu.numDepartement === "099" ? "99" : lieu.numDepartement!
    const circoCode =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      departementCode + lieu.numCirco!.padStart(5 - departementCode.length, "0")
    return [circoCode, depute]
  }),
)

export function mandatDeputeActifFromDepute(
  depute: Acteur,
): Mandat | undefined {
  return depute.mandats?.filter(
    (mandat) =>
      mandat.xsiType === TypeMandat.MandatParlementaireType &&
      mandat.typeOrgane === CodeTypeOrgane.Assemblee &&
      mandat.dateDebut.toString() <= now &&
      (!mandat.dateFin || mandat.dateFin.toString() >= now),
  )[0]
}
