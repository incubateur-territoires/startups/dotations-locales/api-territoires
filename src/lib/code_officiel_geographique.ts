export interface CollectiviteOutreMer {
  /**
   * Code de la collectivité d'outre-mer
   */
  COMER: string
  /**
   * Type de nom en clair
   */
  TNCC: TypeNomEnClair
  /**
   * Libellé en lettres majuscules sans accents
   */
  NCC: string
  /**
   * Libellé en caractères majuscules, minuscules et accentués.)
   */
  NCCENR: string
  /**
   * Libellé en caractères majuscules, minuscules et accentués avec article
   */
  LIBELLE: string
}

export interface Commune {
  /**
   * Type de commune
   */
  TYPECOM: TypeCommune
  /**
   * Code commune
   */
  COM: string
  /**
   * Code région. Absent pour les communes associées ou déléguées
   */
  REG?: string
  /**
   * Code département. Absent pour les communes associées ou déléguées
   */
  DEP?: string
  /**
   * Code de la collectivité territoriale ayant les compétences départementales. Absent pour les communes associées ou déléguées
   */
  CTCD?: string
  /**
   * Code arrondissement. Absent pour les communes associées ou déléguées
   */
  ARR?: string
  /**
   * Type de nom en clair
   */
  TNCC: TypeNomEnClair
  /**
   * Nom en clair (majuscules)
   */
  NCC: string
  /**
   * Nom en clair (typographie riche)
   */
  NCCENR: string
  /**
   * Nom en clair (typographie riche) avec article
   */
  LIBELLE: string
  /**
   * Code canton. Pour les communes « multi-cantonales » code décliné de 99 à 90 (pseudo-canton) ou de 89 à 80 (communes nouvelles). Absent pour les communes associées ou déléguées
   */
  CAN?: string
  /**
   * Code de la commune parente pour les arrondissements municipaux et les communes associées ou déléguées.
   */
  COMPARENT?: string
}

export interface CommuneCollectiviteOutreMer {
  /**
   * Code du zonage
   */
  COM_COMER: string
  /**
   * Type de nom en clair
   */
  TNCC: TypeNomEnClair
  /**
   * Libellé en lettres majuscules sans accents
   */
  NCC: string
  /**
   * Libellé en caractères majuscules, minuscules et accentués.)
   */
  NCCENR: string
  /**
   * Libellé en caractères majuscules, minuscules et accentués avec article
   */
  LIBELLE: string
  /**
   * Code Nature Zonage
   */
  NATURE_ZONAGE: NatureZonage
  /**
   * Code de la collectivité d’outre-mer
   */
  COMER: string
  /**
   * Libellé de la collectivité d’outre-mer
   */
  LIBELLE_COMER: string
}

export interface Departement {
  /**
   * Code département
   */
  DEP: string
  /**
   * Code région
   */
  REG: string
  /**
   * Code de la commune chef-lieu
   */
  CHEFLIEU: string
  /**
   * Type de nom en clair
   */
  TNCC: TypeNomEnClair
  /**
   * Nom en clair (majuscules)
   */
  NCC: string
  /**
   * Nom en clair (typographie riche)
   */
  NCCENR: string
  /**
   * Nom en clair (typographie riche) avec article
   */
  LIBELLE: string
}

export interface EvenementCommune {
  /**
   * Type d'évènement de communes
   */
  MOD: TypeEvenementCommune
  /**
   * Date d'effet (AAAA-MM-JJ)
   */
  DATE_EFF: string
  /**
   * Type de la commune avant évènement
   */
  TYPECOM_AV: TypeCommune
  /**
   * Code de la commune avant évènement
   */
  COM_AV: string
  /**
   * Type de nom en clair
   */
  TNCC_AV: TypeNomEnClair
  /**
   * Nom en clair (majuscules)
   */
  NCC_AV: string
  /**
   *Nom en clair (typographie riche)
   */
  NCCENR_AV: string
  /**
   * Nom en clair (typographie riche) avec article
   */
  LIBELLE_AV: string
  /**
   * Type de la commune après évènement
   */
  TYPECOM_AP: TypeCommune
  /**
   * Code de la commune après évènement
   */
  COM_AP: string
  /**
   * Type de nom en clair
   */
  TNCC_AP: TypeNomEnClair
  /**
   * Nom en clair (majuscules)
   */
  NCC_AP: string
  /**
   *Nom en clair (typographie riche)
   */
  NCCENR_AP: string
  /**
   * Nom en clair (typographie riche) avec article
   */
  LIBELLE_AP: string
}

export type NatureZonage =
  | "COM" // Commune
  | "DIS" // District
  | "CIR" // Circonscription territoriale
  | "CPT" // Code à 5 chiffres de Clipperton

export type TypeCommune =
  | "COM" // Commune
  | "COMA" // Commune associée
  | "COMD" // Commune déléguée
  | "ARM" // Arrondissement municipal

export type TypeEvenementCommune =
  | 10 // Changement de nom
  | 20 // Création
  | 21 // Rétablissement
  | 30 // Suppression
  | 31 // Fusion simple
  | 32 // Création de commune nouvelle
  | 33 // Fusion association
  | 34 // Transformation de fusion association en fusion simple
  | 35 // Suppression de commune déléguée
  | 41 // Changement de code dû à un changement de département
  | 50 // Changement de code dû à un transfert de chef-lieu
  | 70 // Transformation de commune associé en commune déléguée

export type TypeNomEnClair =
  | 0 // Pas d'article et le nom commence par une consonne sauf H muet ; charnière = DE
  | 1 // Pas d'article et le nom commence par une voyelle ou un H muet ; charnière = D'
  | 2 // Article = LE ; charnière = DU
  | 3 // Article = LA ; charnière = DE LA
  | 4 // Article = LES ; charnière = DES
  | 5 // Article = L' ; charnière = DE L'
  | 6 // Article = AUX ; charnière = DES
  | 7 // Article = LAS ; charnière = DE LAS
  | 8 // Article = LOS ; charnière = DE LOS

export function assertNeverTncc(tncc: never) {
  return `Unhandled TNCC ${tncc}`
}

export function libelleSimplifieFromTnccAndNcc(
  tncc: TypeNomEnClair,
  ncc: string,
): string {
  switch (tncc) {
    case 0:
    case 1:
      return ncc
    case 2:
      return "LE " + ncc
    case 3:
      return "LA " + ncc
    case 4:
      return "LES " + ncc
    case 5:
      return "L " + ncc
    case 6:
      return "AUX " + ncc
    case 7:
      return "LAS " + ncc
    case 8:
      return "LOS " + ncc
    default:
      assertNeverTncc(tncc)
      return ncc
  }
}

export function libelleSimplifieWithCharniereFromTnccAndNcc(
  tncc: TypeNomEnClair,
  ncc: string,
): string {
  switch (tncc) {
    case 0:
      return "DE " + ncc
    case 1:
      return "D " + ncc
    case 2:
      return "DU " + ncc
    case 3:
      return "DE LA " + ncc
    case 4:
      return "DES " + ncc
    case 5:
      return "DE L " + ncc
    case 6:
      return "DES " + ncc
    case 7:
      return "DE LAS " + ncc
    case 8:
      return "DE LOS " + ncc
    default:
      assertNeverTncc(tncc)
      return "DE " + ncc
  }
}

export function libelleWithCharniereFromTnccAndNccenr(
  tncc: TypeNomEnClair,
  nccenr: string,
): string {
  switch (tncc) {
    case 0:
      return "de " + nccenr
    case 1:
      return "d'" + nccenr
    case 2:
      return "du " + nccenr
    case 3:
      return "de la " + nccenr
    case 4:
      return "des " + nccenr
    case 5:
      return "de l'" + nccenr
    case 6:
      return "des " + nccenr
    case 7:
      return "de las " + nccenr
    case 8:
      return "de los " + nccenr
    default:
      assertNeverTncc(tncc)
      return "de " + nccenr
  }
}
