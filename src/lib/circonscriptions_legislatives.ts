import fs from "fs-extra"

export interface CirconscriptionLegislative {
  /**
   * Code de la circonscription (sur 5 caractères)
   * Les 3 premiers caractères sont constitués du code du département
   * (complété par un 0 pour les départements métropolitains) suivi
   * d'un numéro de circonscription de deux chiffres.
   * Dans le cas des circonscriptions contenant des communes nouvelles, il
   * est possible que des communes nouvelles aient été créées en fusionnant
   * des communes de départements différents. Dans ce cas la commune sera
   * rattachée à deux circonscriptions appartenant à deux départements
   * différents. Mais il n'empêche que chaque circonscription reste dans un
   * seul département.
   */
  code: string
  /**
   * Codes des communes de la circonscription
   *
   * Note : Certaines communes nouvelles sont constituées à partir de communes
   * situées dans des départements différents => Ces commune sont rattachées
   * à des circonscriptions situées dans des départements différents.
   *
   * Note : les circonscriptions des Français établis hors de France n'ont pas
   * de communes.
   */
  codes_communes?: string[]
  /**
   * Libellé de la circonscription (typographie riche)
   */
  libelle: string
}

export interface CirconscriptionLegislativeMinistereInterieur {
  "CODE DPT": number | string
  "NOM DPT": string
  "CODE COMMUNE": number
  "NOM COMMUNE": string
  "CODE CIRC LEGISLATIVE": number
  "CODE CANTON": number
  "NOM CANTON": string
}

export interface CirconscriptionLegislativeRow {
  /**
   * Code du départerment
   */
  DEP: string
  /**
   * Nom en clair du département (typographie riche)
   */
  libdep: string
  /**
   * Code de la région
   */
  REG: string
  /**
   * Nom en clair de la région (typographie riche)
   */
  libreg: string
  /**
   * Code de la commune
   */
  COMMUNE_RESID: string
  /**
   * Nom en clair de la commune (typographie riche)
   */
  LIBcom: string
  /**
   * Code de la circonscription
   */
  circo: string
  /**
   * Type d'inclusion de la commune dans la circonscription
   */
  type_com?:
    | "entière" // Commune entièrement incluse dans la circonscription
    | "partiel" // Commune partiellement incluse dans la circonscription
}

const circonscriptionsLegislatives = (await fs.readJson(
  "static/circonscriptions_legislatives.json",
)) as CirconscriptionLegislative[]
export const circonscriptionLegislativeByCode: {
  [code: string]: CirconscriptionLegislative
} = Object.fromEntries(
  circonscriptionsLegislatives.map((circo) => [circo.code, circo]),
)

export function departementCodeFromCirconscriptionLegislativeCode(
  code: string,
) {
  const departementCode = code.slice(0, 2)
  return ["97", "98"].includes(departementCode)
    ? code.slice(0, 3)
    : departementCode
}
