import fs from "fs-extra"

export interface Epci {
  /**
   * Code de l'EPCI
   */
  EPCI: string
  /**
   * Nom en clair (typographie riche)
   */
  LIBEPCI: string
  /**
   * Nature de l'EPCI
   */
  NATURE_EPCI:
    | "CA" // Communauté d'agglomération
    | "CC" // Communauté de communes
    | "CU" // Communauté urbaine
    | "ME" // Métropole
  /**
   * Codes des communes constituant l'EPCI
   */
  COM: string[]
}

const epciEntries = (await fs.readJson("static/epci.json")) as Epci[]
export const epciByCode: {
  [code: string]: Epci
} = Object.fromEntries(epciEntries.map((epci) => [epci.EPCI, epci]))
