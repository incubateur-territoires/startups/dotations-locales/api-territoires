export interface AutocompletionBase {
  code: string
  index: number
  libelle: string
}

export interface CirconscriptionLegislativeAutocompletion
  extends CommuneAutocompletion {
  autocompletion1?: string
  autocompletion2?: string
  autocompletion3?: string
  autocompletion4?: string
  autocompletion6?: string
  autocompletion12?: string
  role?: "commune" | "député" | "distribution postale" | "lieu-dit"
}

export interface CommuneAutocompletion extends AutocompletionBase {
  autocompletion1?: string
  autocompletion2?: string
  autocompletion3?: string
  autocompletion4?: string
  autocompletion6?: string
  autocompletion12?: string
  role?: "distribution postale" | "lieu-dit"
}

export interface DepartementAutocompletion extends AutocompletionBase {
  autocompletion1?: string
  autocompletion2?: string
  autocompletion3?: string
  autocompletion4?: string
  autocompletion6?: string
  autocompletion12?: string
  role?: "commune" | "distribution postale" | "lieu-dit"
}

export interface EpciAutocompletion extends AutocompletionBase {
  autocompletion1?: string
  autocompletion2?: string
  autocompletion3?: string
  autocompletion4?: string
  autocompletion6?: string
  autocompletion12?: string
  role?: "commune" | "distribution postale" | "lieu-dit"
}
