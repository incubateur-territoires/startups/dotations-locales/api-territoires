# LexImpact Territoires

## _Diverses API pour les territoires français : autocomplétion des communes, etc_

## Installation

```sh
git clone https://git.leximpact.dev/leximpact/territoires.git
cd territoires/
npm install
```

### Mise à jour des données sur les territoires

#### Base officielle des codes postaux

cf https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/

```sh
mkdir -p data
wget "https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=json&timezone=Europe/Berlin&lang=fr" -O data/laposte_hexasmal.json
npx tsx src/scripts/import_laposte_hexasmal.ts
npx prettier --write data/laposte_hexasmal.json
```

#### Code officiel géographique

- [Code officiel géographique au premier janvier 2022](https://www.insee.fr/fr/information/6051727)
- [Code officiel géographique au premier janvier 2023](https://www.insee.fr/fr/information/6800675)

```sh
mkdir -p data
```

##### Fichier des communes

```sh
# wget "https://www.insee.fr/fr/statistiques/fichier/6051727/commune_2022.csv" -O data/commune.csv
wget "https://www.insee.fr/fr/statistiques/fichier/6800675/v_commune_2023.csv" -O data/commune.csv
npx tsx src/scripts/import_code_officiel_geographique_commune.ts
```

##### Fichier des communes des collectivités d'outre-mer

```sh
# wget "https://www.insee.fr/fr/statistiques/fichier/6051727/com_comer_2022.csv" -O data/com_comer.csv
wget "https://www.insee.fr/fr/statistiques/fichier/6800675/v_commune_comer_2023.csv" -O data/com_comer.csv
npx tsx src/scripts/import_code_officiel_geographique_com_comer.ts
```

##### Fichier des évènements sur les communes

```sh
# wget "https://www.insee.fr/fr/statistiques/fichier/6051727/mvtcommune_2022.csv" -O data/mvtcommune.csv
wget "https://www.insee.fr/fr/statistiques/fichier/6800675/v_mvtcommune_2023.csv" -O data/mvtcommune.csv
npx tsx src/scripts/import_code_officiel_geographique_mvtcommune.ts
```

##### Fichier des départements

```sh
# wget "https://www.insee.fr/fr/statistiques/fichier/6051727/departement_2022.csv" -O data/departement.csv
wget "https://www.insee.fr/fr/statistiques/fichier/6800675/v_departement_2023.csv" -O data/departement.csv
npx tsx src/scripts/import_code_officiel_geographique_departement.ts
```

##### Fichier des collectivités d'outre-mer

```sh
# wget "https://www.insee.fr/fr/statistiques/fichier/6051727/comer_2022.csv" -O data/comer.csv
wget "https://www.insee.fr/fr/statistiques/fichier/6800675/v_comer_2023.csv" -O data/comer.csv
npx tsx src/scripts/import_code_officiel_geographique_comer.ts
```

#### Fichier des intercommunalités (établissements publics de coopération intercommunale, EPCI)

cf https://www.insee.fr/fr/information/2510634

```sh
cd data/
wget "https://www.insee.fr/fr/statistiques/fichier/2510634/Intercommunalite_Metropole_au_01-01-2022.zip"
unzip Intercommunalite_Metropole_au_01-01-2022.zip
rm Intercommunalite_Metropole_au_01-01-2022.zip
mv Intercommunalite_Metropole_au_01-01-2022.xlsx epci.xlsx
cd ../
npx tsx src/scripts/import_epci.ts
```

#### Circonscriptions législatives

#### Fichier Insee des circonscriptions législatives

cf https://www.insee.fr/fr/statistiques/6436476

Note : il manque dans le fichier les collectivités d'outre-mer (COM) et Mayotte (les autres départements et régions d'outre-mer (DROM) sont présents).

```sh
wget "https://www.insee.fr/fr/statistiques/fichier/6436476/circo_composition.xlsx" -O data/circo_composition.xlsx
npx tsx src/scripts/import_insee_circonscriptions_legislatives.ts
```

#### Fichier des circonscriptions législatives provenant du Ministère de l'intérieur.

cf https://www.data.gouv.fr/fr/datasets/circonscriptions-legislatives-table-de-correspondance-des-communes-et-des-cantons-pour-les-elections-legislatives-de-2012-et-sa-mise-a-jour-pour-les-elections-legislatives-2017/

Le fichier intéressant est `Table_de_correspondance_circo_legislatives2017-1.xlsx`.

Note : Les codes des communes de ce fichier sont plus complexes à utiliser que le fichier de l'Insee ci-dessus. Il n'est donc utilisé que pour les collectivités d'outre-mer + Mayotte et complète celui de l'Insee.

```sh
wget "https://www.data.gouv.fr/fr/datasets/r/4d0b70e1-7757-43cc-882b-5c3b04fe38b4" -O data/circonscriptions_legislatives.xlsx
npx tsx src/scripts/import_ministere_interieur_circonscriptions_legislatives_collectivites_outre-mer.ts
```

#### Fichiers des députés

```sh
cd data/
git clone https://git.en-root.org/tricoteuses/data/assemblee-nettoye/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye.git
cd ../
npx tsx src/scripts/import_deputes.ts
```

#### Critères de répartition des dotations aux collectivités de la Direction générale des collectivités locales

Ces fichiers des critères de répartition des dotations aux collectivités contient un découpage territorial, mis à jour fin Mars, début Avril, plus à jour que le code officiel géographique de l'Insee.

cf :

- http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php
- https://www.dotations-dgcl.interieur.gouv.fr/consultation/dotations_en_ligne.php

```sh
# Critères de répartition des dotations communales :
wget "http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition_csv.php?annee=2022&ct=com" -O data/criteres_repartition_communes.xls
npx tsx src/scripts/import_dgcl_criteres_repartition_communes.ts
# Critères de répartition des dotations communales :
wget "http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition_csv.php?annee=2022&ct=dep" -O data/criteres_repartition_departements.xls
npx tsx src/scripts/import_dgcl_criteres_repartition_departements.ts
```

#### Autocomplétions

#### Autocomplétions des communes

```sh
npx tsx src/scripts/generate_communes_autocompletions.ts
```

#### Autocomplétions des départements

```sh
npx tsx src/scripts/generate_departements_autocompletions.ts
```

#### Autocomplétions des intercommunalités (EPCI)

```sh
npx tsx src/scripts/generate_epci_autocompletions.ts
```

#### Autocomplétions des circonscriptions législatives

```sh
npx tsx src/scripts/generate_circonscriptions_legislatives_autocompletions.ts
```

#### Autocomplétions des communes DGCL

```sh
npx tsx src/scripts/generate_communes_dgcl_autocompletions.ts
```

#### Autocomplétions des départements DGCL

```sh
npx tsx src/scripts/generate_departements_dgcl_autocompletions.ts
```

#### Autocomplétions des intercommunalités (EPCI) DGCL

```sh
npx tsx src/scripts/generate_epci_dgcl_autocompletions.ts
```

## Lancement du serveur

### En développement

```sh
npm run dev
```

### En production

```sh
npm run build
```

## Utilisation

### Recherche d'une commune par autocomplétion

```sh
wget --header "Accept: application/json" "http://localhost:2999/communes/autocomplete?q=saint%20python"
wget --header "Accept: application/json" "http://localhost:2999/communes/autocomplete?q=st%20python"
wget --header "Accept: application/json" "http://localhost:2999/communes/autocomplete?q=75007%20Paris"
wget --header "Accept: application/json" "http://localhost:2999/communes/autocomplete?q=Paris%207"
wget --header "Accept: application/json" "http://localhost:2999/communes/autocomplete?field=commune&q=Paris%207"
```

Plus d'exemples se trouvent dans les [tests](./tests).
